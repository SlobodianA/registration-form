<?php

session_start();
// ініціалізація змінних
$email = '';
$firstname = '';
$lastname = '';
$password = '';
$base = '';
$errors = [];
$_SESSION['success'] = false;

//якщо форма була відправленна перевіряємо вхідні дані
if(isset($_REQUEST['submit'])){

    //видалення зайвих символів і запис даних у змінні
    $email = deleteUnexpectedChars($_REQUEST['email']);
    $firstname = deleteUnexpectedChars($_REQUEST['firstname']);
    $lastname = deleteUnexpectedChars($_REQUEST['lastname']);
    $password = deleteUnexpectedChars($_REQUEST['password']);
    $base = $_REQUEST['base'];

    // отримання даних про курс валют
    $currencies = ['USD', 'PLN', 'EUR', 'RUB', 'GBP'];
    if (($key = array_search($base, $currencies)) !== false) {
        unset($currencies[$key]);
    }
    $currencies = implode(',', $currencies);
    $options = [
        "base=$base",
        "symbols=$currencies"
    ];
    $url = 'https://api.exchangeratesapi.io/';
    $currencyRates = getCurlResponse($url, 'latest', $options);
    $currencyAPIResult = json_decode($currencyRates, true);

    // отримання даних про погоду
    $weatherOptions = [
        "q=Ternopil",
        "APPID=fe3b557b84a157a5d5d91dd81825dfa6",
        "units=metric",
        "mode=HTML",
    ];
    $weatherURL = 'http://api.openweathermap.org/data/2.5/';
    $weatherJSON = getWeatherResponse($weatherURL, 'weather', $weatherOptions);
    $weatherResult = json_decode($weatherJSON, true);

    // перевірка імейлу на валідність та перевірка довжини полів firstname i lastname, та перевірка на їхню схожість
    // якщо дані введено неправильно відповідні помилки запишуться у масив errors
    checkEmail($email);
    checkLength($firstname,'First name', 3);
    checkLength($lastname, 'Last name', 3);
    if($firstname === $lastname){
        $errors[] = 'First name and last name must be different';
    }

    // перевірка пароля на валідність
    checkPassword($password);

    // якщо помилок немає, перейти на іншу сторінку
    if (empty($errors)){
        $_SESSION['firstname'] = $firstname;
        $_SESSION['lastname'] = $lastname;
        $_SESSION['email'] = $email;
        $_SESSION['password'] = $password;
        $_SESSION['currency_api_result'] = $currencyAPIResult;
        $_SESSION['current_weather'] = $weatherResult;
        $_SESSION['success'] = true;
        header('Location: succes.php');
    }
}

// Блок з функціями
function checkPassword($password)
{
    global $errors;

    checkLength($password,'Password',6);
    if(!preg_match('/[A-Z]/', $password)) {
        $errors[] = 'Password must contain at least 1 uppercase symbol';
    }
    if(!preg_match('/[a-z]/', $password)) {
        $errors[] = 'Password must contain at least 1 lowercase symbol';
    }
    if(!preg_match('/[\\d]/', $password)) {
        $errors[] = 'Password must contain at least 1 numeric symbol';
    }
}

function checkEmail($email)
{
    global $errors;

    if(!preg_match('/.+@.+\..+/i', $email)){
        $errors[]= 'Enter correct email';
    }
}

function checkLength ($field, $name, $min, $max = 30)
{
    global $errors;

    $result = ((mb_strlen($field, 'UTF-8') < $min) || (mb_strlen($field, 'UTF-8') > $max));

    if($result == true){
        $errors[] = "Length of field $name must be greater than $min";
    }
}

function deleteUnexpectedChars($field)
{
    $field = trim($field);
    $field = stripslashes($field);
    $field = strip_tags($field);
    $field = htmlspecialchars($field);

    return $field;
}

function getCurlResponse($url, $function = '', $options = array())
{
    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => $url . $function . '?' . implode('&', $options),
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING => "",       // handle all encodings
        CURLOPT_AUTOREFERER => true,     // set referer on redirect
        CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
    ]);

    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}

function getWeatherResponse($url, $function = '', $options = array())
{
    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => $url . $function . '?' . implode('&', $options),
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING => "",       // handle all encodings
        CURLOPT_AUTOREFERER => true,     // set referer on redirect
        CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
    ]);

    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}