<?php require_once 'formcheck.php'?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="src/css/bootstrap.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="src/js/bootstrap.js"></script>
    <title>Registration</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-6 ml-4 mt-2">
            <h1>Registration form</h1>
            <?php if (!empty($errors)):?>
                <?php foreach ($errors as $error):?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error!</strong> <?=$error?>.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <form action="" method="post">
                <div class="form-group">
                    <label for="Email">Email address</label>
                    <input type="email" name="email" class="form-control" id="Email" value="<?= $email?>"
                           placeholder="Enter email" >
                    <div class="invalid-feedback">Enter correct email</div>
                </div>
                <div class="form-group">
                    <label for="Firstname">First name</label>
                    <input type="text" name="firstname" class="form-control" id="Firstname" value="<?= $firstname?>"
                           placeholder="Enter first name" >
                    <div class="invalid-feedback">Enter your first name</div>
                </div>
                <div class="form-group">
                    <label for="Lastname">Last name</label>
                    <input type="text" name="lastname" class="form-control" id="Lastname" value="<?= $lastname?>"
                           placeholder="Enter last name" >
                    <div class="invalid-feedback">Enter your last name</div>
                </div>
                <div class="form-group">
                    <label for="Password">Password</label>
                    <input type="password" name="password" class="form-control" id="Password" placeholder="Password">
                    <div class="invalid-feedback">Enter your password</div>
                </div>
                <div class="form-group">
                    <label for="base">Your base currency</label>
                    <select class="form-control" id="base" name="base">
                        <option>USD</option>
                        <option>PLN</option>
                        <option>EUR</option>
                        <option>RUB</option>
                        <option>GBP</option>
                    </select>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
