<?php
    session_start();

    if(empty($_SESSION['success'])){
        header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="src/js/bootstrap.js"></script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Custom Theme files -->
    <link href="src/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="src/css/bootstrap.css">
    <!-- //Custom Theme files -->
    <!-- web font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'><!--web font-->
    <link href='//fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!-- //web font -->
    <!-- js -->
    <script src="src/js/jquery-1.9.1.min.js"></script>
    <title>Success</title>
</head>
<body onload="startTime()">
<div class="container-fluid">
    <div class="row">
        <div class="col-6 ml-4 mt-2">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Succes!</strong> You successfully registered.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <h2>Your registration data</h2>
            <p><strong>Email: </strong><?= $_SESSION['email']; ?></p>
            <p><strong>First name: </strong><?= $_SESSION['firstname']; ?></p>
            <p><strong>Last name: </strong><?= $_SESSION['lastname']; ?></p>
            <p><strong>Password: </strong><?= $_SESSION['password'] ?></p>
            <h3>Today currencies rates: </h3>
            <?php foreach (array_reverse($_SESSION['currency_api_result']) as $key => $value):?>
                <?php if($key == 'base'):?>
                    <p><strong>Your base currency: </strong> <?=$value?></p>
                <?php endif;?>
                <?php if ($key == 'rates'):?>
                    <?php foreach ($value as $key => $rate):?>
                        <p><strong>Rate: </strong> <?=$key .' - '. $rate?></p>
                    <?php endforeach;?>
                <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="col-5 mt-2">
            <h1>Weather</h1>

                <div class="agileits-top">
                    <div class="agileinfo-top-row">
                        <div class="agileinfo-top-img">
                            <span> </span>
                        </div>
                        <h3><?= round($_SESSION['current_weather']['main']['temp'])?><sup class="degree">°</sup><span>C</span></h3>
                        <p><?= $_SESSION['current_weather']['name']?></p>
                        <div class="agileinfo-top-time">
                            <div class="date-time">
                                <div class="dmy">
                                    <div id="txt"></div>
                                    <div class="date">
                                        <!-- Date-JavaScript -->
                                        <script type="text/javascript">
                                            var mydate=new Date()
                                            var year=mydate.getYear()
                                            if(year<1000)
                                                year+=1900
                                            var day=mydate.getDay()
                                            var month=mydate.getMonth()
                                            var daym=mydate.getDate()
                                            if(daym<10)
                                                daym="0"+daym
                                            var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
                                            var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
                                            document.write(""+dayarray[day]+", "+montharray[month]+" "+daym+", "+year+"")
                                        </script>
                                        <!-- //Date-JavaScript -->
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w3ls-bottom">
                    <h2>Today's Weather - <?=$_SESSION['current_weather']['weather'][0]['main']?></h2>
                    <p><strong>Clouds: </strong><span><?= $_SESSION['current_weather']['clouds']['all']?> %</span></p>
                    <p><strong>Wind: </strong><span><?= $_SESSION['current_weather']['wind']['speed']?> m/s</span></p>
                    <p><strong>Humidity: </strong><span><?= $_SESSION['current_weather']['main']['humidity']?> %</span></p>
                    <p><strong>Presure: </strong><span><?= $_SESSION['current_weather']['main']['pressure']?> hpa.</span></p>
                </div>
            </div>
        </div>
    </div>

<script>
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i}; // add zero in front of numbers < 10
        return i;
    }
</script>
</body>
</html>